// Shadow from picture, Copyright (C) 2012 Oleg Kochkin. License GPL.

#include <QPainter>
#include "math.h"

QImage getShadow(QImage img, int &sigma, QColor color);
